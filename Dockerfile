# Use the official Rust image as the base image
FROM rust:latest as builder

# Set the working directory
WORKDIR /usr/src/week4-mini-project

# Copy the project files into the container
COPY . .

# Build the Actix Web app
RUN cargo build --release

# Expose the port that Actix Web will run on
EXPOSE 8080

# Define the default command to run
CMD cargo run
