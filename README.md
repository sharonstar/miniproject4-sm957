# Week4 Project

Requirements: Containerize a Rust Actix Web Service
* Containerize simple Rust Actix web app
* Build Docker image
* Run container locally

# Steps
* install docker following the steps in the offical website
* create a new project   `cargo new myproject`
* create your own code in /src/main.rs file
* add dependencies to Cargo.toml file. actix-web = "4" is necessary to for a Actix Web Service
* run your app using  `cargo run`
* write a dockerfile to build your own image from a basic image
* build the docker image using `docker build -t yourimagename`
* run the docker container and set the port using `docker run -p 8080:8080 yourimagename`

# Screenshot
* build the docker image

![](img1.png)
* view all the docker images

![](img2.png)
* run the docker container

![](img3.png)
* view all the docker containers

![](img4.png)
